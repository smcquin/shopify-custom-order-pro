class Status < ActiveRecord::Base
  validates :name, :note, presence: true
end
