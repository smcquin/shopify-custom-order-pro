class OrdersController < AuthenticatedController
  def index
    @orders = ShopifyAPI::Order.find(:all, params: {
      order: [params[:field], params[:order]].join(' '),
      status: params[:status]
    })

    @paginated_orders = Kaminari.paginate_array(@orders)
                                .page(params[:page])
                                .per(10)
  end

  def show
    @order = ShopifyAPI::Order.find(params[:id])
  end

  def new
    @order = ShopifyAPI::Order.new
  end

  def create
    if not params[:order][:products].blank?
      params[:order][:products].each do |key, value|
        @product = ShopifyAPI::Product.new
        @product.title     = value[:name]
        @product.body_html = value[:description]
        @product.variants  = [{price: value[:price]}]
        # Upload picture if exists
        uploaded_io = value[:picture]
        unless uploaded_io.blank?
          @picture_name = "#{Time.now.to_i}#{File.extname(uploaded_io.original_filename)}"
          product_picture = File.open(Rails.root.join('public', 'uploads', @picture_name), 'wb') do |file|
            file.write(uploaded_io.read)
          end
          @product.images = [{src: "#{root_url}uploads/#{@picture_name}"}]
        end
        @product.save
        # Remove file from disc
        File.delete(Rails.root.join('public', 'uploads', @picture_name))
      end
    end

    respond_to do |format|
      format.html { redirect_to root_url, notice: 'Order was successfully created.' }
    end

    # respond_to do |format|
    #   if @order.save
    #     format.html { redirect_to @order, notice: 'Order was successfully created.' }
    #     format.json { render :show, order: :created, location: @order }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @order.errors, order: :unprocessable_entity }
    #   end
    # end
  end

  def show_data
  end
end
