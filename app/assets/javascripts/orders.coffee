# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  # $('#new_order').submit (e) ->
  #   e.preventDefault()
  #   console.log JSON.stringify($(this).serializeObject())
  #   false

  # Remove Product
  $('#new_order').on 'click', '.remove-product', (e) ->
    e.preventDefault()
    $(this).parent('fieldset').remove()

  # Add Product
  $('.add-product').click (e) ->
    e.preventDefault() 
    createProductForm($("fieldset").last())

# Generate Product fields
createProductForm = (el) ->
  len = $('.product-information').length
  
  form = '<fieldset class="product-information">'+
    '<legend>Products Info</legend>'+
    '<a class="remove-product btn btn-danger pull-right" href="#"><i class="icon-trash"></i> Remove this product</a>'+
    '<div class="clearfix"></div>'+
    '<div class="form-group">'+
      '<div class="col-sm-3">'+
        '<a id="product-selector" class="btn btn-primary" href="#">Select Products</a>'+
      '</div>'+
      '<div class="col-sm-5">'+
        '<input type="text" name="order[products]['+len+'][name]" id="order_products_name_" class="form-control" placeholder="Product name" required="required">'+
      '</div>'+
      '<div class="col-sm-2">'+
        '<input type="text" name="order[products]['+len+'][price]" id="order_products_price_" class="form-control" placeholder="00.00" required="required">'+
      '</div>'+
      '<div class="col-sm-2">'+
        '<input type="text" name="order[products]['+len+'][quantity]" id="order_products_quantity_" value="1" class="form-control" required="required">'+
      '</div>'+
    '</div>'+
    '<div class="form-group">'+
      '<label class="col-sm-2 control-label" for="order_product_description">Product description</label>'+
      '<div class="col-sm-10">'+
        '<textarea name="order[products]['+len+'][description]" id="order_products_description_" class="form-control" placeholder="Product description"></textarea>'+
      '</div>'+
    '</div>'+
    '<div class="form-group">'+
      '<label class="col-sm-2 control-label" for="order_product_picture">Product picture</label>'+
      '<div class="col-sm-10">'+
        '<input type="file" name="order[products]['+len+'][picture]" id="order_products_picture_">'+
      '</div>'+
    '</div>'+
  '</fieldset>';

  $(form).insertAfter(el)


$.fn.serializeObject = ->
  o = {}
  a = @serializeArray()
  $.each a, ->
    if o[@name] != undefined
      if !o[@name].push
        o[@name] = [ o[@name] ]
      o[@name].push @value or ''
    else
      o[@name] = @value or ''
    return
  o