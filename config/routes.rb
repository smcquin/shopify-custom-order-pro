Rails.application.routes.draw do

  resources :orders do
    post :show_data, on: :collection
  end
  resources :statuses

  mount ShopifyApp::Engine, at: '/'
  
  # api routes
  get '/api/i18n/:locale' => 'api#i18n'

  root :to => 'orders#index'
end
